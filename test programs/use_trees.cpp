#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include "../sd/linked_list.h"
#include "../sd/bst.h"
#include "../sd/treap.h"

#define NUM_ELEMS 20000

template<typename TreeNodeType> void test_simple_bst(BinarySearchTree<int, TreeNodeType > *bst, std::string msg, int worst_case) {
	fprintf(stderr, "\n\n");
	srand(1000);
	LinkedList<int> ll;

	int i, tstart = clock();
	for (i = 0; i < NUM_ELEMS; i++) {
		int k;
		if (worst_case)
			k = 2 * i + 1;
		else
			k = rand() % NUM_ELEMS;
		bst->insert_info(k);
		ll.addLast(k);
	}
	fprintf(stderr, "%s: After insertions\n", msg.c_str());

	struct list_elem<int> *p = ll.plast;
	for (i = NUM_ELEMS - 1; i >= 0; i--) {
		if (bst->find_info(p->info) == NULL) {
			fprintf(stderr, "ERROR! Element %d was not found in the tree\n", 2 * i + 1);
		}
		p = p->prev;
	}

	p = ll.pfirst;
	for (i = 0; i < NUM_ELEMS; i++) {
		bst->remove_info(p->info);
		p = p->next;
	}

	fprintf(stderr, "%s: After removals\n", msg.c_str());
	fprintf(stderr, "%s: Duration=%.3lf sec\n", msg.c_str(), (double) (clock() - tstart) / CLOCKS_PER_SEC);
}

template<typename TreeNodeType> void test_treap(Treap<int, TreeNodeType> *bst, std::string msg, int worst_case) {
	fprintf(stderr, "\n\n");
	srand(1000);
	LinkedList<int> ll;

	int i, tstart = clock();
	for (i = 0; i < NUM_ELEMS; i++) {
		int k;
		if (worst_case)
			k = 2 * i + 1;
		else
			k = rand() % NUM_ELEMS;
		bst->insert_info(k);
		ll.addLast(k);
	}
	fprintf(stderr, "%s: After insertions\n", msg.c_str());

	struct list_elem<int> *p = ll.plast;
	for (i = NUM_ELEMS - 1; i >= 0; i--) {
		if (bst->find_info(p->info) == NULL) {
			fprintf(stderr, "ERROR! Element %d was not found in the tree\n", 2 * i + 1);
		}
		p = p->prev;
	}

	p = ll.pfirst;
	for (i = 0; i < NUM_ELEMS; i++) {
		bst->remove_info(p->info);
		p = p->next;
	}

	fprintf(stderr, "%s: After removals\n", msg.c_str());
	fprintf(stderr, "%s: Duration=%.3lf sec\n", msg.c_str(), (double) (clock() - tstart) / CLOCKS_PER_SEC);
}

int main() {
	BinarySearchTree<int, BSTNode<int> > bst(compare_int);
	test_simple_bst<BSTNode<int> >(&bst, "Standard BST (random)", 0);
	BinarySearchTree<int, BSTNode<int> > bst2(compare_int);
	test_simple_bst<BSTNode<int> >(&bst2, "Standard BST (worst case)", 1);

	Treap<int, TreapNode<int> > bst3(compare_int);
	test_treap<TreapNode<int> >(&bst3, "Treap (random)", 0);
	Treap<int, TreapNode<int> > bst4(compare_int);
	test_treap<TreapNode<int> >(&bst4, "Treap (worst case)", 1);

	return 0;
}
